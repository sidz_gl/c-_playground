#include "pch.h"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>


void Page156()
{
	std::cout << "Pointer Prastise!" << std::endl;
	int m_iNumber = 10;
	float m_fFractionNumber = 10.2f;
	char m_cCharacter = '7';

	int *pNumber = NULL;
	float *pFractionNumber = NULL;
	char *pCharacter = NULL;


	pNumber = &m_iNumber;
	pCharacter = &m_cCharacter;
	pFractionNumber = &m_fFractionNumber;

	std::cout << "Address of Number:0x" << std::hex << pNumber << std::endl;
	std::cout << "Address of FractionNumber:0x" << std::hex << pFractionNumber << std::endl;


	std::cout << "Value of Number:" << std::dec << *pNumber << std::endl;
	std::cout << "Value of FractionNumber:" << std::dec << *pFractionNumber << std::endl;
	std::cout << "Value of Character:" << *pCharacter << std::endl;

	std::cout << "Enter a Number:";
	std::cin >> *pNumber;
	std::cout << "Modified Value of Pointer REferenced Number:" << std::dec << *pNumber << std::endl;
	std::cout << "Modified Value of Number:" << std::dec << m_iNumber << std::endl;


	std::cout << "Size of Pointer Number:" << sizeof(pNumber) << std::endl;
	std::cout << "Size of Pointer FractionNumber:" << sizeof(pFractionNumber) << std::endl;
	std::cout << "Size of Pointer:" << sizeof(pCharacter) << std::endl;

}

void Page175()
{
	int *pNumber = new int;
	std::cout << "Enter a Number" << std::endl;
	std::cin >> *pNumber;
	std::cout << "Entered the number!" << *pNumber << ",Location," << std::hex << pNumber;

	std::string m_strName = "";

	std::cout << "Enter a Name" << std::endl;
	std::cin >> m_strName;

	char *CopyOfName = new char[m_strName.length() + 1];
	CopyOfName = strcpy(CopyOfName, m_strName.c_str());
	std::cout << "Dynamically Created Character!" << CopyOfName << std::endl;

	delete[] CopyOfName;
}

void TestingPointer()
{
	int m_iNumber = 5;
	int *piNum1;
	int *piNum2;

	piNum1 = new int;
	//piNum2 = new int;

	//piNum1 = &m_iNumber;
	*piNum1 = 5;
	piNum2 = piNum1;
	std::cout << "TEsting Pointer:Value:" << m_iNumber << std::endl;
	std::cout << "TEsting Pointer:piNum1:" << *piNum1 << std::endl;
	std::cout << "TEsting Pointer:piNum2:" << *piNum2 << std::endl;
	if (piNum1 != NULL)
		delete piNum1;

	//*piNum2 = 6;

	std::cout << "after TEsting Pointer:piNum1:" << piNum1 << std::endl;
	std::cout << "after TEsting Pointer:piNum2:" << *piNum2 << std::endl;


}


class SimpleNames
{
public:
	std::string m_strName;


	void SetName(const std::string &a_strName)
	{
		m_strName = a_strName;
	}
	std::string GetName()
	{
		return m_strName;
	}


};
void TestingConstStuff()
{
	int iNumber = 5;
	int iNumber2 = 10;

	const int *pNum1 = &iNumber;
	std::cout << "Constant DATA(VAlue):" << *pNum1 << std::endl;
	std::cout << "Constant DATA(ADDress):" << &pNum1 << std::endl;

	//*pNum1 = 55;//CONST value!
	pNum1 = &iNumber2;
	std::cout << "AFTER Constant DATA(VAlue):" << *pNum1 << std::endl;
	std::cout << "AFTER Constant DATA(ADDress):" << &pNum1 << std::endl;


	int* const pNum2 = &iNumber;
	std::cout << "Constant POINTER(VAlue):" << *pNum2 << std::endl;
	std::cout << "Constant POINTER(ADDress):" << &pNum2 << std::endl;
	//pNum2 = &iNumber2;//CONST POINTER
	*pNum2 = 10;

	std::cout << "Constant POINTER(VAlue):" << *pNum2 << std::endl;
	std::cout << "Constant POINTER(ADDress):" << &pNum2 << std::endl;
}


//PAGE 477
void Order(SimpleNames **a_aryPointer)
{
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			if ((*(a_aryPointer + i))->GetName() > (*(a_aryPointer + j))->GetName())
			{
				SimpleNames * temp_Different = (*(a_aryPointer + i));
				(*(a_aryPointer + i)) = (*(a_aryPointer + j));
				(*(a_aryPointer + j)) = temp_Different;
			}
		}
	}
}
void Print(SimpleNames **a_aryPointer)
{
	std::cout << "Priting:" << std::endl;
	for (int i = 0; i < 5; i++)
	{
		std::cout << "Sorted:" << (*(a_aryPointer + i))->GetName() << std::endl;
	}
}


void TestingSorting()
{
	SimpleNames *aryNames[5];

	for (int i = 0; i < 5; i++)
	{
		aryNames[i] = new SimpleNames;
		aryNames[i]->SetName(std::to_string(i) + "_Name");
		std::cout << "Values:" << aryNames[i]->GetName() << std::endl;
	}
	Order(aryNames);


	Print(aryNames);

}
int main()
{
	TestingSorting();
	//TestingConstStuff();

	//Page156();
	//Page175();
	//TestingPointer();
	//TestingSorting();
	return 0;
}


